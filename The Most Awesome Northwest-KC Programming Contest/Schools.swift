//
//  DataModels.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Pradeep Kolli on 3/14/19.
//  Copyright © 2019 northwest. All rights reserved.
//

import Foundation
class Schools{
    
    static let shared = Schools()
    private var schools:[School]
    
    private convenience init(){
        self.init(schools:[])
    }
    private init(schools:[School]){
        self.schools = schools
    }
    func numSchools() -> Int{
        return schools.count
    }
    subscript(index:Int) -> School {
        return schools[index]
    }
    func add(school:School){
        schools.append(school)
    }
    func delete(school:School){
        for sch in 0..<schools.count{
            if schools[sch] == school{
                schools.remove(at:sch)
            }
        }
        
    }
}
