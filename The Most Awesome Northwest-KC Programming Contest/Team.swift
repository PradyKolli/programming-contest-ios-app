//
//  Team.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Pradeep Kolli on 3/14/19.
//  Copyright © 2019 northwest. All rights reserved.
//

import Foundation
class Team{
    var name:String
    var students:[String]
    
    init(name: String, students: [String]){
        self.name = name
        self.students = students
    }
    
}
